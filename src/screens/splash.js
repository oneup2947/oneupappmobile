import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  AsyncStorage
} from 'react-native';
import { StackNavigator,NavigationActions } from 'react-navigation';

import Constants from './../constants/Constants';

import App from './../../App';
import Login from './login';
import Schedule from './schedule';
import SelectCategoryList from './selectCategoryList';
import SelectAccountList from './selectAccountList';

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;


export class Splash extends Component{

  componentWillMount(){
    var that = this;
    AsyncStorage.getItem('token', (err, result) => {
         var routeName = '';
         if(result == null){
           routeName = 'Login';
         }else {
           Constants.TOKEN = result;
           routeName = 'Details'
         }
         const resetAction = NavigationActions.reset({
           index: 0,
           actions: [NavigationActions.navigate({routeName: routeName})]
         });
         that.props.navigation.dispatch(resetAction);
    });
  }
  render(){
    return(
      <View style={{width:screenWidth, height:screenHeight, alignItems:'center', justifyContent:'center'}}>
        <Image source={require('./../images/logo.png')} style={{width:140,height:140}}/>
      </View>
    );
  }
}

export default StackNavigator({
  Home: {
    screen: Splash,
  },
  Details:{
    screen: App
  },
  Login: {
    screen: Login
  },
  Schedule: {
    screen: Schedule
  },
  SelectCategoryList: {
    screen: SelectCategoryList
  },
  SelectAccountList: {
    screen: SelectAccountList
  }

},
{
  headerMode:'none'
});
