import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Alert,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  FlatList
} from 'react-native';
import { Container, Content,Header, Left, Body, Right, Title, Icon, Button, Tab, Tabs,Form, Picker, Label, Thumbnail,Textarea,Footer, FooterTab } from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

import LoadingDialog from './../dialogs/loadingDialog';

import axios from 'axios';
import {CachedImage} from 'react-native-cached-image';


export default class Create extends Component{
  constructor(props){
    super(props);
    this.state={
      content:'',
      postType:'',
      imageUrl:'',
      videoUrl:'',
      sourceUrl: '',
      isMediaLoading:false,
      multiplePost:[]
    }
  }

  componentDidMount(){

  }

  showAlert = () => {
    if(this.state.imageUrl != ''){
      this.setState({imageUrl:'',videoUrl:'', postType:0});
      this.renderMedia();
      return;
    }
    Alert.alert(
      'Select an option',
      '',
      [
        {text: 'Select Image from Gallery', onPress: () => this.selectImage()},
        {text: 'Select Video from Gallery', onPress: () => this.selectVideo()},
        {text: 'Use Camera', onPress: () => this.openCamera()},
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}
      ],
      { cancelable: true }
    )
  }

  selectImage = () => {
    var that = this;
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      this.processUpload(image);
    }).catch((error) =>{
    });
  }

  selectVideo = () => {
    var that = this;
    ImagePicker.openPicker({
      mediaType: "video"
    }).then(image => {
      this.processUpload(image);
    }).catch((error) =>{
    });
  }

  openCamera = () => {
    var that = this;
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      this.processUpload(image);
    }).catch((error) =>{
    });
  }

  pickMultipleImages = () => {
    var that = this;
    ImagePicker.openPicker({
      multiple: true
    }).then(image => {
      for(var i =0; i < image.length; i++){
        that.processMultipleUpload(image[i], image.length, i);
      }
      //that.processMultipleUpload();
    }).catch((error) =>{
    });
  }

  processUpload = (image) => {
    var that = this;
    var photo = {
        uri: image.path,
        type: image.mime,
        name: Date.now().toString(),
    };
    var formData = new FormData();
    formData.append('file',photo);
    that.setState({isMediaLoading: true});
    formData.append('upload_preset','k3tw0us8');
    axios({
        url:'http://api.cloudinary.com/v1_1/dgkqns6fw/upload',
        method:'POST',
        headers:{
          'Content-Type':'application/x-www-form-urlencoded'
        },
        data:formData
    }).then(function(response){
      that.setState({isMediaLoading: false});
      response.data.url = response.data.url.includes("https")?response.data.url:response.data.url.replace("http", "https");
      if(response.data.resource_type == "image"){
        that.setState({postType:0, imageUrl: response.data.url}, function(){
          that.renderMedia;
        });
      }
      else
      {
        that.setState({postType:1,
          videoUrl: response.data.url,
          imageUrl: response.data.url.substr(0,response.data.url.lastIndexOf(".")) +".jpg"}, function(){
            that.renderMedia;
        });
      }
    }).catch((error) => {console.log(error);that.setState({isMediaLoading: false})});;
  }

  processMultipleUpload = (image, length , count) => {
    var that = this;
    var photo = {
        uri: image.path,
        type: image.mime,
        name: Date.now().toString(),
    };
    var formData = new FormData();
    formData.append('file',photo);
    that.setState({isMediaLoading: true});
    formData.append('upload_preset','k3tw0us8');
    axios({
        url:'https://api.cloudinary.com/v1_1/dgkqns6fw/upload',
        method:'POST',
        headers:{
          'Content-Type':'application/x-www-form-urlencoded'
        },
        data:formData
    }).then(function(response){
      response.data.url = response.data.url.includes("https")?response.data.url:response.data.url.replace("http", "https");
      if(response.data.resource_type == "image"){
        var temp = that.state.multiplePost;
        that.setState({multiplePost:[]});
        var obj = {};
        obj.id = count;
        obj.content='';
        obj.postType = 0;
        obj.imageUrl = response.data.url;
        obj.videoUrl = '';
        obj.sourceUrl = '';
        temp.push(obj);
        that.setState({multiplePost: temp});
      }
      else
      {
        alert("Videos are not supported for multiple posts");
      }
      if(that.state.multiplePost.length == length)
       that.setState({isMediaLoading: false});

    }).catch((error) => {alert("We are facing some issue in uploading this media. Please try later.");that.setState({isMediaLoading: false})});
  }

  goToSchedule = () => {

    if(this.state.content == '' && this.state.imageUrl == '')
    {
      alert("You cannot schedule an empty post");
      return;
    }

    var obj ={};
    obj.content = this.state.content;
    obj.postType =  this.state.postType;
    obj.imageUrl = this.state.imageUrl;
    obj.videoUrl = this.state.videoUrl;

    var match = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.exec(this.state.content);
    if(match == undefined || match == null)
      obj.sourceUrl = '';
    else {
      obj.sourceUrl = match[0]
    }

    var data = [];
    data.push(obj);
    this.props.navigation.navigate('Schedule',{postData: data});
  }

  goToScheduleMultiple = () => {
      this.props.navigation.navigate('Schedule',{postData: this.state.multiplePost});
  }

  onValueChange(value: string) {
    this.setState({
      selected1: value
    });
  }

  renderMedia = () => {
    if(this.state.imageUrl == '')
     return <Image source={require('./../images/add_icon.png')} style={{width:40,height:40}}/>;
    else {
     return  <Image source={{uri: this.state.imageUrl}} style={{marginTop:40,width:100,height:100,marginBottom:40,borderRadius:4}} resizeMode="cover"/>
    }
  }

  renderContent = () => {
    if(this.state.multiplePost.length == 0)
    return <View style={{width:screenWidth-40,alignItems:"center"}}>
              <Button onPress={this.pickMultipleImages} block style={{backgroundColor:'rgb(79,169,244)', marginTop:50}}>
                <Text style={{color:'#fff'}}>Select Images</Text>
              </Button>
              <Text style={{fontSize:13,color:'rgb(79,169,244)',marginTop:10}}>Max 25 Images</Text>
              <Text style={{fontSize:13,color:'rgb(79,169,244)',marginTop:10}}>(Long press the image to enable multiple selection.)</Text>

            </View>
    else
      return <FlatList
      removeClippedSubviews={true}
      keyExtractor={(item, index) => item.id}
      data={this.state.multiplePost}
      renderItem={this._renderItem}
      keyExtractor={(item, index) => index.toString()}
    />
  }

  updateDesc = (text, item) => {
    var that = this;
    var temp = this.state.multiplePost;
    var obj = temp[item.item.id];
    obj.content = text;

    var match = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.exec(text);
    if(match == undefined || match == null)
      obj.sourceUrl = '';
    else {
      obj.sourceUrl = match[0]
    }

    this.setState({multiplePost:[]});
    this.setState({multiplePost:temp});
  }

  renderFooter = () => {
      if(this.state.multiplePost.length !=0)
        return <Footer>
                <FooterTab>
                  <Button onPress={this.goToScheduleMultiple}>
                    <Text>Schedule</Text>
                  </Button>
                </FooterTab>
              </Footer>
      else
        return null;

  }

  _renderItem = (item) => {
    return <View style={{flexDirection:'row',width:screenWidth-10,borderBottomWidth:1, borderBottomColor:'#fff',minHeight:140,alignItems:'center'}}>
      <CachedImage source={{uri: item.item.imageUrl}} resizeMode="cover" style={{width:80,height:80,borderRadius:4,marginRight:10}}/>
      <Textarea style={{marginBottom:10, width: screenWidth-130}} onChangeText={(text) =>this.updateDesc(text,item)} rowSpan={5} bordered placeholder="Enter description or links.." />
    </View>
  }
  render(){
    return(
      <Container>
        <Header hasTabs>
        <Left>
          <Button transparent>
            <Icon name='menu' onPress={() => {this.props.navigation.navigate('DrawerToggle')}} />
          </Button>
          </Left>
          <Body>
            <Title>Create Post</Title>
          </Body>
          <Right />
        </Header>
        <View style={{backgroundColor:'rgb(234,239,242)', width: screenWidth, height: screenHeight-120}}>
          <Tabs initialPage={0}>
            <Tab heading="Single Post" style={{backgroundColor:'rgb(234,239,242)'}}>
              <Content style={{marginTop:20}}>
                <Label style={{color:'rgb(79,169,244)', marginLeft:10, marginTop:20,fontSize:14}}>Description</Label>
                <View style={{width: screenWidth, alignItems:'center', marginTop:2}}>
                  <View style={{width:screenWidth-16, flexWrap: 'wrap'}}>
                   <Textarea onChangeText={(text) =>{this.setState({content: text})}} rowSpan={5} bordered placeholder="Enter description or links.." />
                  </View>
                </View>
                <TouchableWithoutFeedback onPress={this.showAlert}>
                  <View style={{width: screenWidth, alignItems:'center', marginTop:15}}>
                    <View style={{width:screenWidth-16,flexDirection:'row',flexWrap: 'wrap', minHeight:40, alignItems:'center'}}>
                      {this.renderMedia()}
                      <Text style={{fontSize:14,color:'rgb(21,126,251)', marginLeft:10}}>{this.state.imageUrl==''?'Add Media':'Delete Media'}</Text>
                    </View>
                  </View>
                </TouchableWithoutFeedback>

                <View style={{width: screenWidth, alignItems:'center', marginTop:40}}>
                  <View style={{width:screenWidth-32, alignItems:'center'}}>
                    <Button onPress={this.goToSchedule} block style={{backgroundColor:'rgb(79,169,244)'}}>
                      <Text style={{color:'#fff'}}>Next</Text>
                    </Button>
                  </View>
                </View>
              </Content>
            </Tab>
            <Tab heading="Multiple Post" style={{backgroundColor:'rgb(234,239,242)'}}>
              <View style={{width:screenWidth, alignItems:'center'}}>
                {this.renderContent()}
              </View>
            </Tab>
          </Tabs>
        </View>
        {this.renderFooter()}
        <LoadingDialog isVisible={this.state.isMediaLoading} displayText="Uploading Media..."/>
      </Container>
    );
  }
}
