import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Alert,
  Text,
  View,
  FlatList,
  Image,
  TouchableWithoutFeedback,
  Dimensions
} from 'react-native';
import { Container, Content,Header, Left, Body, Right, Title, Icon, Button, Tab, Tabs,Form, List,ListItem, Label, Thumbnail,Textarea } from 'native-base';

var screenWidth = Dimensions.get('window').width;
import {NavigationActions } from 'react-navigation';
var networkCall = require('./../network/networkCall');


export default class SelectCategoryList extends Component{
  constructor(props){
    super(props);
    const {state} = props.navigation;
    this.state ={
      categories: state.params.categories,
      onPressCategory: state.params.onPressCategory

    }
  }

  setCategory = (item) => {
    this.state.onPressCategory(item);
    this.props.navigation.pop();
  }

  _renderItem = (item) => {
    return  <TouchableWithoutFeedback onPress={()=>this.setCategory(item)}><ListItem><Text>{item.item.category_name}</Text></ListItem></TouchableWithoutFeedback>
  }

  render(){
    return(
      <Container>
      <Header>
        <Left>
          <Button onPress={() => {this.props.navigation.dispatch(NavigationActions.back())}} transparent>
            <Icon name='arrow-back' />
          </Button>
        </Left>
        <Body>
          <Title>Select Category</Title>
        </Body>
        <Right />
      </Header>
      <FlatList
       removeClippedSubviews={true}
        data={this.state.categories}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => index.toString()}
       />
      </Container>
    );
  }
}
