import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Alert,
  Text,
  View,
  FlatList,
  Image,
  TouchableWithoutFeedback,
  Dimensions
} from 'react-native';
import { Container, Content,Header, Left, Body, Right, Title, Icon, Button, Tab, Tabs,Form, List,ListItem, Label, Thumbnail,Textarea,Switch} from 'native-base';

var screenWidth = Dimensions.get('window').width;
import {NavigationActions } from 'react-navigation';
var networkCall = require('./../network/networkCall');


export default class SelectAccountList extends Component{
  constructor(props){
    super(props);
    const {state} = props.navigation;
    this.state ={
      accounts: state.params.accounts,
      onPressAccount: state.params.onPressAccount

    }
  }

  setAccount = (item) => {
    var tempArr = this.state.accounts;
    this.state.accounts.forEach(function(i,index){
      if(tempArr[index].id == item.item.id)
        tempArr[index].selected = !tempArr[index].selected;
    });
    this.state.onPressAccount(tempArr);
    this.setState({accounts: []}, function(){
      this.setState({accounts: tempArr});
    });


    // this.props.navigation.pop();
  }

  _renderItem = (item) => {
    return <TouchableWithoutFeedback onPress={()=>this.setAccount(item)}>
      <ListItem>
        <Left>
          <Text>{item.item.name}</Text>
        </Left>
        <Right>
          <Switch onValueChange={()=>this.setAccount(item)} value={item.item.selected} />
        </Right>
      </ListItem>
    </TouchableWithoutFeedback>
  }

  render(){
    return(
      <Container>
      <Header>
        <Left>
          <Button onPress={() => {this.props.navigation.dispatch(NavigationActions.back())}} transparent>
            <Icon name='arrow-back' />
          </Button>
        </Left>
        <Body>
          <Title>Select Account</Title>
        </Body>
        <Right />
      </Header>
      <FlatList
       removeClippedSubviews={true}
        data={this.state.accounts}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => index.toString()}
       />
      </Container>
    );
  }
}
