import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  AsyncStorage,
  FlatList,
  ActivityIndicator
} from 'react-native';
import { Container, Content,Header, Left, Body, Right, Title, Icon, Button, Tab, Tabs } from 'native-base';
import moment from 'moment';


import { DrawerNavigator } from 'react-navigation';
import {NavigationActions } from 'react-navigation';


import Constants from './../constants/Constants';

//import all post types
import TextOnly from './../commons/posttype/textonly';
import TextImage from './../commons/posttype/textimage';

var screenWidth = Dimensions.get('window').width;
var networkCall = require('./../network/networkCall');

import NetworkDialogue from './../dialogs/networkDialogue';


export default class Queue extends Component{
  static navigationOptions = {
    drawerLabel: 'Queue',
  };

  constructor(props){
    super(props);
    this.state = {
      networkError: false,
      scheduledPosts:[],
      publishedPosts:[],
      failedPosts:[],
      isScheduledPostEnd: false,
      isPublishedPostEnd: false,
      isFailedPostEnd: false
    }
  }

  componentWillMount(){
    this.getScheduledPosts();
    this.getPublishedPosts();
    this.getFailedPosts();
  }

  getScheduledPosts = () => {
    var that = this;
    var mid_url = 'scheduledposts'+'?start='+this.state.scheduledPosts.length;
    networkCall.onHttpRequest(mid_url, false).then(function(response) {
      if(response.data.code != undefined && (response.data.code == 400 || response.data.code == 401)){
        that.deAuthenticateUser();
      }else{
        that.parseData(response.data, 0);
      }
    }, function(error){
      that.setState({networkError: true});
    });
  }

  getPublishedPosts = () => {
    var that = this;
    var mid_url = 'publishedposts' +'?start='+this.state.publishedPosts.length;;
    networkCall.onHttpRequest(mid_url, false).then(function(response) {
      if(response.data.code != undefined && (response.data.code == 400 || response.data.code == 401)){
        that.deAuthenticateUser();
      }else{
        that.parseData(response.data, 1);
      }
    }, function(error){
      that.setState({networkError: true});
    });
  }

  getFailedPosts = () => {
    var that = this;
    var mid_url = 'failedposts' +'?start='+this.state.failedPosts.length;;
    networkCall.onHttpRequest(mid_url, false).then(function(response) {
      if(response.data.code != undefined && (response.data.code == 400 || response.data.code == 401)){
        that.deAuthenticateUser();
      }else{

        that.parseData(response.data, 2);
      }
    }, function(error){
      that.setState({networkError: true});
    });
  }

  parseData = (data, type) => {
    //this.scheduledPosts = data.scheduledPosts;
    if(type == 0){
      if(data.scheduledPosts.length == 0){
        this.setState({isScheduledPostEnd: true});
        return;
      }
      var temp = this.state.scheduledPosts;
      temp = temp.concat(data.scheduledPosts);
      this.setState({scheduledPosts: temp});
    }
    if(type == 1){
      if(data.publishedPosts.length == 0){
        this.setState({isPublishedPostEnd: true});
        return;
      }
      var temp = this.state.publishedPosts;
      temp = temp.concat(data.publishedPosts);
      this.setState({publishedPosts: temp});
    }
    if(type == 2){
      if(data.failedPosts.length == 0){
        this.setState({isFailedPostEnd: true});
        return;
      }
      var temp = this.state.failedPosts;
      temp = temp.concat(data.failedPosts);
      this.setState({failedPosts: temp});
    }

  }

  deAuthenticateUser = () => {
    alert("Your session has expired. Please login again.");
    //AsyncStorage.setItem('token', null);
    AsyncStorage.clear();
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Login'})]
    });
    this.props.navigation.dispatch(resetAction);
  }

  _renderItem = (item) => {
    var item = item.item;
    if(item.content_image == "NA")
      return <TextOnly desc= {item.content}
                       networkName={item.social_network_name}
                       networkType={item.social_network_type}
                       categoryName={item.category_name}
                       dateTime={moment(item.date_time, "YYYY-MM-DD hh:mm").format('MMM DD, YYYY, h:mm A')}
                       />
    else
      return <TextImage desc= {item.content}
                       image={item.content_image}
                       networkName={item.social_network_name}
                       networkType={item.social_network_type}
                       categoryName={item.category_name}
                       dateTime={moment(item.date_time, "YYYY-MM-DD hh:mm").format('MMM DD, YYYY, h:mm A')}
                       />
  }

  renderFooter = (type) => {
    if(type == 0) return this.returnScheduledFooter();
    if(type == 1) return this.returnPublishedFooter();
    if(type == 2) return this.returnFailedFooter();
  }

  endReached = (type) => {
    if(type == 0){
      if(this.state.isScheduledPostEnd) return;
      this.getScheduledPosts();
    }

    if(type == 1){
      if(this.state.isPublishedPostEnd) return;
      this.getPublishedPosts();
    }

    if(type == 2){
      if(this.state.isFailedPostEnd) return;
      this.getFailedPosts();
    }

  }

  returnScheduledFooter = () => {
    if(this.state.isScheduledPostEnd==false)
      return (
        <View
          style={{
            paddingVertical: 20,
            borderTopWidth: 1,
            borderColor: "#CED0CE"
          }}
        >
          <ActivityIndicator animating color="rgb(79,169,244)" />
        </View>
      );
    else {
      return (
        <View
          style={{
            paddingVertical: 20,
            borderTopWidth: 1,
            alignItems:'center',
            borderColor: "#CED0CE"
          }}
        >
          <Text style={{fontSize:12, fontWeight:'bold',marginLeft:5,color:'rgb(124,123,127)'}}>No posts to show.</Text>
        </View>
      );
    }
  }

  returnPublishedFooter = () => {
    if(this.state.isPublishedPostEnd==false)
      return (
        <View
          style={{
            paddingVertical: 20,
            borderTopWidth: 1,
            borderColor: "#CED0CE"
          }}
        >
          <ActivityIndicator animating color="rgb(79,169,244)" />
        </View>
      );
    else {
      return (
        <View
          style={{
            paddingVertical: 20,
            borderTopWidth: 1,
            alignItems:'center',
            borderColor: "#CED0CE"
          }}
        >
          <Text style={{fontSize:12, fontWeight:'bold',marginLeft:5,color:'rgb(124,123,127)'}}>No posts to show.</Text>
        </View>
      );
    }
  }

  returnFailedFooter = () => {
    if(this.state.isFailedPostEnd==false)
      return (
        <View
          style={{
            paddingVertical: 20,
            borderTopWidth: 1,
            borderColor: "#CED0CE"
          }}
        >
          <ActivityIndicator animating color="rgb(79,169,244)" />
        </View>
      );
    else {
      return (
        <View
          style={{
            paddingVertical: 20,
            borderTopWidth: 1,
            alignItems:'center',
            borderColor: "#CED0CE"
          }}
        >
          <Text style={{fontSize:12, fontWeight:'bold',marginLeft:5,color:'rgb(124,123,127)'}}>No posts to show.</Text>
        </View>
      );
    }
  }

  renderError (){
    if(this.state.networkError == true)
     return <NetworkDialogue isVisible={true} />
  }


  render() {
    return (
      <Container>
        <Header hasTabs>
        <Left>
          <Button transparent>
            <Icon name='menu' onPress={() => {this.props.navigation.navigate('DrawerToggle')}} />
          </Button>
          </Left>
          <Body>
            <Title>Queue</Title>
          </Body>
          <Right />
        </Header>
          <Tabs initialPage={0}>
            <Tab heading="Queue" style={{backgroundColor:'rgb(234,239,242)'}}>
              <View style={{width:screenWidth,borderRadius:2,alignItems:'center'}}>
                <FlatList
                  removeClippedSubviews={true}
                  onEndReached={() => { this.endReached(0) }}
                  ListFooterComponent={this.renderFooter(0)}
                  keyExtractor={(item, index) => item.id}
                  data={this.state.scheduledPosts}
                  renderItem={this._renderItem}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </Tab>
            <Tab heading="Published" style={{backgroundColor:'rgb(234,239,242)'}}>
              <View style={{width:screenWidth,borderRadius:2,alignItems:'center'}}>
                <FlatList
                  removeClippedSubviews={true}
                  onEndReached={() => { this.endReached(1) }}
                  ListFooterComponent={this.renderFooter(1)}
                  keyExtractor={(item, index) => item.id}
                  data={this.state.publishedPosts}
                  renderItem={this._renderItem}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </Tab>
            <Tab heading="Failed" style={{backgroundColor:'rgb(234,239,242)'}}>
              <View style={{width:screenWidth,borderRadius:2,alignItems:'center'}}>
                <FlatList
                  removeClippedSubviews={true}
                  onEndReached={() => { this.endReached(2)}}
                  ListFooterComponent={this.renderFooter(2)}
                  keyExtractor={(item, index) => item.id}
                  data={this.state.failedPosts}
                  renderItem={this._renderItem}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </Tab>
          </Tabs>
        {this.renderError()}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  icon: {
    width: 24,
    height: 24,
  }
});
