import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Alert,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  Dimensions
} from 'react-native';
import { Container, Content,Header, Left, Body, Right, Title, Icon, Button, Tab, Tabs,Form, List,ListItem, Label, Thumbnail,Textarea } from 'native-base';

var screenWidth = Dimensions.get('window').width;
import {NavigationActions } from 'react-navigation';
import LoadingDialog from './../dialogs/loadingDialog';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Prompt from 'rn-prompt';
import moment from 'moment';




var networkCall = require('./../network/networkCall');



export default class Schedule extends Component{
  constructor(props){
    super(props);
    const {state} = props.navigation;
    this.finalData = [];

    this.state = {
      isCategoriesLoaded: false,
      isSavingPosts: false,
      categories:[],
      selectedCategoryId:-1,
      selectedCategoryName:'',
      accountsForThisCategory:[],
      accountsSelectedNum: 0,
      posts: state.params.postData,
      isDateTime: false,
      isMinutePrompt: false,
      intervalInMin:0
    };
  }

  onValueChange(value: string) {
    this.setState({
      selected1: value
    });
  }

  componentWillMount(){
    var that =  this;
    var mid_url = 'getcategoryandaccounts?';
    networkCall.onHttpRequest(mid_url, true).then(function(response) {
      that.setState({categories: response.data.categories, isCategoriesLoaded: true});

    }, function(error){
      alert(error);
    });
  }

  onPressCategory = (item) => {
    this.setState({selectedCategoryId: item.item.id, selectedCategoryName: item.item.category_name});
    var accountNames = item.item.selected_network.split(',');
    var accountTypes = item.item.network_type.split(',');
    var accountId = item.item.network_id.split(',');
    var tempArr = [];

    for(var i = 0 ; i < accountNames.length; i++){
      var obj = {};
      obj.name = accountNames[i];
      obj.id = accountId[i];
      obj.type = accountTypes[i];
      obj.selected = false;
      obj.categoryid = item.item.id;
      tempArr.push(obj);
    }
    this.setState({accountsForThisCategory: tempArr, accountsSelectedNum:0});
  }

  onPressAccount = (item) => {
    var count = 0 ;
    item.forEach(function(i, index){
      if(i.selected)
      count++;
    });
    this.setState({accountsSelectedNum:count, accountsForThisCategory: item});
  }

  schedulePost = () => {
   if(this.state.selectedCategoryId == -1){
     alert("You have not selected any category");
     return;
   }

   if(this.state.accountsSelectedNum == 0){
     alert("You have not selected any social account.");
     return;
   }

   Alert.alert(
     'Select an option',
     '',
     [
       {text: 'Post Now', onPress: () => this.postNow()},
       {text: 'Schedule', onPress: () => this.showDatePicker()},
       {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}
     ],
     { cancelable: true }
   )
  }

  showDatePicker = () => {
    if(this.state.posts.length <=1)
      this.setState({isDateTime: true, isMinutePrompt:false});
    else
      this.setState({isMinutePrompt: true});
  }

  _hideDateTimePicker = () => this.setState({ isDateTime: false });

  _handleDatePicked = (date) => {
    var myDate = new Date(date);
    this.setState({ isDateTime: false});
    this.scheduleNow(myDate);
  };



  postNow = () => {
    var that = this;
    this.setState({isSavingPosts: true});
    var ts =  (moment().unix()) *1000;

    this.state.posts.forEach(function(post, index){
      that.state.accountsForThisCategory.forEach(function(account, index){
        if(account.selected){
          var obj = {};
          obj.scheduledDate =moment(ts).format('YYYY-MM-DD');;
          obj.scheduledTime = moment(ts).format('HH:mm');

          obj.category_id =  account.categoryid;
          obj.social_network_type = account.type;
          obj.social_network_id = account.id;
          obj.social_network_name = account.name;
          obj.content = post.sourceURL == ''?post.content:post.content.replace(post.sourceURL,'');
          obj.postType = post.postType;
          obj.image_url = post.imageUrl==''?'NA':post.imageUrl;
          obj.source_url = post.sourceUrl == ''?'NA':post.sourceUrl;
          obj.video_url = post.videoUrl == ''?'NA':post.videoUrl;
          obj.board_name = null;
          obj.board_id = null;
          obj.isRepin = 0;
          obj.repinId = 0;
          obj.isEvergreen = 0;
          obj.expiry_date = 'NA';
          obj.expiry_after_share = 'NA';
          //console.log(obj);
          that.finalData.push(obj);
        }
      });
      ts =  (moment(ts).add(3, 'minutes').unix()) *1000;
    });
    that.saveData(that.finalData);
  }

  scheduleNow = (date) => {
    var ts =  (moment(date).unix()) *1000;
    var that = this;
    this.setState({isSavingPosts: true});
    this.state.posts.forEach(function(post, index){
      that.state.accountsForThisCategory.forEach(function(account, index){
        if(account.selected){
          var obj = {};
          obj.scheduledDate =moment(ts).format('YYYY-MM-DD');;
          obj.scheduledTime = moment(ts).format('HH:mm');

          obj.category_id =  account.categoryid;
          obj.social_network_type = account.type;
          obj.social_network_id = account.id;
          obj.social_network_name = account.name;
          obj.content = post.sourceURL == ''?post.content:post.content.replace(post.sourceURL,'');
          obj.postType = post.postType;
          obj.image_url = post.imageUrl==''?'NA':post.imageUrl;
          obj.source_url = post.sourceUrl == ''?'NA':post.sourceUrl;
          obj.video_url = post.videoUrl == ''?'NA':post.videoUrl;
          obj.board_name = null;
          obj.board_id = null;
          obj.isRepin = 0;
          obj.repinId = 0;
          obj.isEvergreen = 0;
          obj.expiry_date = 'NA';
          obj.expiry_after_share = 'NA';
          //console.log(obj);
          that.finalData.push(obj);
        }
      });
      ts =  (moment(ts).add(that.state.intervalInMin, 'minutes').unix()) *1000;
    });
    that.saveData(that.finalData);
  }

  saveData = (data) => {
    var that = this;
    console.log(data);
    networkCall.onPostRequest('schedulepost', data).then(function(response){
        //go to queueView
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'Details'})]
        });
        that.props.navigation.dispatch(resetAction);
    }, function(error){
      that.setState({isSavingPosts: false});
      alert(error);
      //alert("Some error occured while saving your posts. Please try again.");
    });
  }

  render(){
    return(
      <Container>
        <Header hasTabs>
          <Left>
            <Button onPress={() => {this.props.navigation.dispatch(NavigationActions.back())}} transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Select Category</Title>
          </Body>
          <Right />
        </Header>
        <Content style={{backgroundColor:'rgb(234,239,242)'}}>
          <TouchableWithoutFeedback onPress={() => {this.props.navigation.navigate('SelectCategoryList',{ categories: this.state.categories,onPressCategory:(item)=>this.onPressCategory(item)})}}>
            <View>
              <Label style={{color:'rgb(79,169,244)', marginLeft:10, fontSize:14, marginTop:25}}>{this.state.selectedCategoryId==-1?'Select Category': this.state.selectedCategoryName}</Label>
              <View style={{width: screenWidth, alignItems:'center', marginTop:4}}>
                <View style={{width:screenWidth-16, flexWrap: 'wrap'}}>
                  <Text style={{marginTop:0, color:'',fontSize:12,color:'rgb(124,123,127)'}}>Click to {this.state.selectedCategoryId==-1?'select':'change'} category for this post.</Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => {if(this.state.selectedCategoryId == -1){alert("You have not selected any category.");return;}this.props.navigation.navigate('SelectAccountList',{ accounts: this.state.accountsForThisCategory,onPressAccount:(item)=>this.onPressAccount(item)})}}>
            <View>
              <Label style={{color:'rgb(79,169,244)', marginLeft:10, marginTop:30,fontSize:14}}>{this.state.accountsSelectedNum==0?'Select Accounts': this.state.accountsSelectedNum+" accounts selected"}</Label>
              <View style={{width: screenWidth, alignItems:'center', marginTop:4}}>
                <View style={{width:screenWidth-16, flexWrap: 'wrap'}}>
                  <Text style={{marginTop:0, color:'',fontSize:12,color:'rgb(124,123,127)'}}>Click to {this.state.accountsSelectedNum==0?'select':'change'} Accounts.</Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>

          <View style={{width: screenWidth, alignItems:'center', marginTop:100}}>
            <View style={{width:screenWidth-32, alignItems:'center'}}>
              <Button onPress={this.schedulePost}  block style={{backgroundColor:'rgb(79,169,244)'}}>
                <Text style={{color:'#fff'}}>Schedule</Text>
              </Button>
            </View>
          </View>
        </Content>
        <LoadingDialog isVisible={!this.state.isCategoriesLoaded} displayText="Loading your categories..."/>
        <LoadingDialog isVisible={this.state.isSavingPosts} displayText="Saving your posts..."/>
        <DateTimePicker
          isVisible={this.state.isDateTime}
          mode='datetime'
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
        />
        <Prompt
            title="Enter interval between posts in minutes"
            placeholder="ex: 3"
            defaultValue="3"
            visible={this.state.isMinutePrompt}
            onCancel={ () => this.setState({
              isMinutePrompt: false
            }) }
            onSubmit={ (value) => this.setState({
              isMinutePrompt: false,
              isDateTime: true,
              intervalInMin: value
            }) }/>
      </Container>
    );
  }
}
