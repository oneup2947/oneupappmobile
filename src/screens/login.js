import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  ActivityIndicator,
  AsyncStorage
} from 'react-native';
import { Root,Container, Content,Header, Left, Form, Item, Input, Title, Icon, Button,Label, Body, Toast } from 'native-base';
import LoadingDialog from './../dialogs/loadingDialog';
import {NavigationActions } from 'react-navigation';


var networkCall = require('./../network/networkCall');
import Constants from './../constants/Constants';
var screenWidth = Dimensions.get('window').width;


export default class Login extends Component{
  constructor(props){
    super(props);
    this.state ={
      username :'',
      password :'',
      reqSent: false
    }
  }

  render(){
    return(
      <Root>
        <Container>
          {this.renderHeader()}
          <Content>
            <View style={{width:screenWidth, alignItems:'center'}}>
              <Image source={require('./../images/logo.png')} style={{width:110,height:110, marginTop:30}}/>
            </View>
            <Form style={{marginTop:40}}>
              <Item floatingLabel>
                <Label>Login Email</Label>
                <Input onChangeText={(text) => {this.setState({username:text})}}/>
              </Item>
              <Item floatingLabel last>
                <Label>Password</Label>
                <Input secureTextEntry={true} onChangeText={(text) => {this.setState({password:text})}}/>
              </Item>
              <View style={{width:screenWidth, alignItems:'center'}}>
                <View style={{width:screenWidth-32, marginTop:30}}>
                  <Button onPress={this.authUser} block style={{backgroundColor:'rgb(79,169,244)'}}>
                    <Text style={{color:'#fff'}}>Login</Text>
                  </Button>
                </View>
                <TouchableWithoutFeedback onPress={this.showCreateAccountToast}>
                  <Text style={{marginTop:20,fontSize:14}}>Not a member? Register</Text>
                </TouchableWithoutFeedback>
              </View>
            </Form>
          </Content>
          <LoadingDialog isVisible={this.state.reqSent} displayText="Logging you in"/>
        </Container>
      </Root>
    );
  }

  /*********************************** Internal Methods ***************************************************/
  goToQueue = () => {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Details'})]
    });
    this.props.navigation.dispatch(resetAction);
  }

  showCreateAccountToast = () => {
    Toast.show({
      text: "Go to - www.oneupapp.io - to register a new account",
      buttonText: "Okay",
      duration: 20000
    })
  }

  renderHeader(){
    if(Platform.OS =="ios"){
      return(
        <Header>
          <Body>
            <Title>Login</Title>
          </Body>
        </Header>
      );
    }
  }

  authUser = () => {
    if(this.state.username.trim()=='' || this.state.password.trim() == '')
     return;
    this.setState({reqSent: true});
    var that =  this;
    var mid_url = 'auth/login?&email='+this.state.username+'&password='+this.state.password;
    networkCall.onHttpRequest(mid_url, true).then(function(response) {
      that.setState({reqSent: false})
      if(response.data.code==422){
        alert("Invalid username or password. Please try again.");
      }else{
        Constants.TOKEN = response.data.token;
        AsyncStorage.setItem('token', response.data.token);
        that.goToQueue();
      }
    },function(error){
        that.setState({reqSent: false})
        alert("Error --> "+error);
    });
  }
}

const styles = StyleSheet.create ({
  modal:{
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    alignItems:'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.6)'
  }
});
