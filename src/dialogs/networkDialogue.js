import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  ActivityIndicator,
  BackAndroid
} from 'react-native';
import Modal from 'react-native-root-modal';


export default class NetworkDialogue extends Component{
  constructor(props){
    super(props);
    this.state= {
      isVisible: this.props.isVisible
    }
  }

  componentWillMount() {
    var that = this;
  }

  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', () => {
        if(that.props.isVisible)
          return true;
        return false;
    });
  }

  render(){
    return(
      <Modal style={styles.modal} visible={this.state.isVisible}>
        <View style={{width:288,minHeight:224,elevation:24, backgroundColor:'#fff', justifyContent:'flex-start',alignItems:'center',borderRadius:6, flexDirection:'column'}}>
          <View style={{width:240,flexDirection:'row', height:38, marginTop:24, borderBottomWidth:1, borderBottomColor:'rgba(0, 0, 0, 0.12)'}}>
            <Image source={require('./../images/warning.png')} style={{width:24,height:24}}/>
            <Text allowFontScaling={false} style={{fontSize:16, color:'#3d475b',marginLeft:13}}>Unable to connect</Text>
          </View>
          <View style={{width:240, minHeight:40, justifyContent:'flex-start', alignItems:'center', marginTop:24}}>
            <Text allowFontScaling={false} style={{fontSize:14, color:'#8fa4af'}}>We are not able to connect to our server. Please check your Internet connection.</Text>
          </View>
          <View style={{width:240,marginLeft:24, marginRight:24,  marginTop:32,justifyContent:'center',alignItems:'center'}}>
            <TouchableWithoutFeedback onPress={() => {this.setState({isVisible:false})}}>
              <View style={{backgroundColor:'#23d06d',width:180,height: 40,justifyContent:'center',alignItems:'center',backgroundColor:'rgb(79,169,244)',borderRadius:125}} >
                  <Text allowFontScaling={false} style={{color:'#fff'}}>OK</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
       </Modal>

    )
  }
}

const styles = StyleSheet.create ({
  modal:{
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    alignItems:'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.6)'
  }
});
