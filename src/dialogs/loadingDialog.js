import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  ActivityIndicator,
  BackAndroid
} from 'react-native';
import Modal from 'react-native-root-modal';


export default class LoadingDialog extends Component{
  constructor(props){
    super(props);
  }

  componentWillMount() {
    var that = this;
    BackAndroid.addEventListener('hardwareBackPress', () => {
        if(that.props.isVisible)
          return true;
        return false;
    });
  }

  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', () => {
        if(that.props.isVisible)
          return true;
        return false;
    });
  }

  render(){
    return(
      <Modal style={styles.modal} visible={this.props.isVisible}>
           <View style={{width:312,height:104,elevation:24, backgroundColor:'#fff', justifyContent:'center',alignItems:'center',borderRadius:4, flexDirection:'row'}}>
            <ActivityIndicator color="rgb(79,169,244)" size = "large"/>
            <Text style={{marginLeft:20}}>{this.props.displayText}</Text>
           </View>
       </Modal>

    )
  }
}

const styles = StyleSheet.create ({
  modal:{
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    alignItems:'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.6)'
  }
});
