import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions
} from 'react-native';
import { Container, Content,Header, Left, Body, Right, Title, Icon, Button, Tab, Tabs } from 'native-base';

var screenWidth = Dimensions.get('window').width;

export default class TextOnly extends Component{
  constructor(props){
    super(props);
  }

  render(){
    return(
      <View style={styles.postParent}>
        <View style={styles.accountName}>
          <Image style={{width: 15, height: 15}} source={{uri:'https://www.oneupapp.io/img/'+this.props.networkType+'.png'}} />
          <Text style={{fontSize:12, fontWeight:'bold',marginLeft:5,color:'rgb(124,123,127)'}}>
            {this.props.networkName.length>10?this.props.networkName.substr(0,9)+"...":this.props.networkName}
          </Text>
          <Right>
            <Text style={{fontSize:12, fontWeight:'bold',color:'rgb(124,123,127)'}}>
              {this.props.categoryName.length>10?this.props.categoryName.substr(0,9)+"...":this.props.categoryName}
            </Text>
          </Right>
        </View>
        <View style={[{marginTop:10},styles.contentArea]}>
          <Text style={{fontSize:14,marginLeft:5,color:'rgb(38,38,38)',marginBottom:7}}>
            {this.props.desc}
          </Text>
        </View>
        <View style={styles.dateAndTime}>
          <Text style={{fontSize:12, fontWeight:'bold',marginLeft:5,color:'rgb(124,123,127)'}}>
            {this.props.dateTime}
          </Text>
        </View>
      </View>
    );
  }
}

const styles= StyleSheet.create({
  postParent: {
    width:screenWidth-32,
    minHeight:150,
    backgroundColor:'#fff',
    marginTop:20,
    alignItems:'center'
  },
  accountName: {
    width:screenWidth-64,
    height:35,
    flexDirection:'row',
    alignItems:'center',
    borderBottomWidth:1,
    borderBottomColor:'#eee'
  },
  dateAndTime: {
    width:screenWidth-64,
    height:35,
    borderTopWidth:1,
    borderTopColor:'#eee',
    justifyContent:'center'
  },
  contentArea: {
    width:screenWidth-64,
    minHeight:90
  }
});
