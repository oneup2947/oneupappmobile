import axios from 'axios';
import Constants from './../constants/Constants';

const serverUrl = "https://www.oneupapp.io/api/";

module.exports = {
	onHttpRequest(mid_url, isLogin) {
		return new Promise(function (resolve, reject) {
			let final_url = serverUrl + mid_url + '&token=' + Constants.TOKEN;
			console.log("making call to -" + final_url);
			axios.get(final_url, {timeout: 30000 }).then((responseJSON) => {
				resolve(responseJSON);
			}).catch((error) => { reject(error) });
		});
  },
	onPostRequest(mid_url,data){
		return new Promise(function(resolve, reject){
			var final_url = serverUrl + mid_url;
			console.log("making post call to -" + final_url);
			axios.post(final_url, {data: data, token: Constants.TOKEN}).then((responseJSON) => {
				resolve(responseJSON);
			}).catch((error) => {reject(error) });
		});
	}
}
