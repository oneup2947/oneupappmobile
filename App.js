import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions
} from 'react-native';
import { Container, Content,Header, Left, Body, Right, Title, Icon, Button, Tab, Tabs } from 'native-base';

import { DrawerNavigator } from 'react-navigation';

import Queue from './src/screens/queue';
import Create from './src/screens/create';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  icon: {
    width: 24,
    height: 24,
  }
});

export default DrawerNavigator({
  Queue: {
    screen: Queue,
  },
  Create:{
    screen: Create
  }
});
